﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Modelo Tabla Ciudad ---

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Models.CiudadModels
{
    public class CiudadModels
    {
        [Key]
        public int id_Ciudad { get; set; }
        public string Ciudad { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool estado { get; set; }
    }
}
