﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Modelo Tabla Departamentos ---

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Models.DepartamentosModels
{
    public class DepartamentosModels
    {
        [Key]
        public int id_Departamentos { get; set; }
        public string Departamento { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool estado { get; set; }
    }
}
