﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Modelo Tabla Clientes ---

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Models.ClientesModels
{
    public class ClientesModels
    {
        #region Modelo Tabla Salones
        [Key]
        public int id { get; set; }
        public string identificacion { get; set; }
        public string nombreApellidos { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public int id_Departamentos { get; set; }
        public int id_Ciudad{ get; set; }
        public int edad { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int estado { get; set; }

        #endregion
    }
}
