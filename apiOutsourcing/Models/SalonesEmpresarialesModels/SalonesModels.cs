﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Modelo Tabla Salones ---

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Models.SalonesEmpresarialesModels
{
    public class SalonesModels
    {
        #region Modelo Tabla Salones
        [Key]
        public int id { get; set; }
        public string reserva { get; set; }
        public DateTime fechaEvento { get; set; }
        public int cantidadPersonas { get; set; }
        public int id_Motivo { get; set; }
        public int id_EstadoConfirmadoNoConfirmado { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int estado { get; set; }

        #endregion
    }
}
