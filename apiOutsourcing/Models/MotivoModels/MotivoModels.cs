﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Modelo Tabla Motivo ---

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Models.MotivoModels
{
    public class MotivoModels
    {
        [Key]
        public int id_Motivo { get; set; }
        public string Motivo { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool estado { get; set; }
    }
}
