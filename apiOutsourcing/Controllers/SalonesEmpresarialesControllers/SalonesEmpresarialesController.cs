﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Controlador SalonesEmpresariales ---

using apiOutsourcing.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Controllers.SalonesEmpresarialesControllers
{
    [Route("api/Salones")]
    [ApiController]
    public class SalonesEmpresarialesController : ControllerBase
    {
        // GET: api/<SalonesEmpresarialesController>
        private readonly AppDbContext context;

        public SalonesEmpresarialesController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet(Name = "GetSalones")]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Salones.ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost(Name = "postSalones")]
        public ActionResult Post([FromBody] Models.SalonesEmpresarialesModels.SalonesModels model)
        {
            try
            {
                context.Salones.Add(model);
                context.SaveChanges();

                return CreatedAtRoute("GetSalones", new { id = model.id }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Models.SalonesEmpresarialesModels.SalonesModels model)
        {
            try
            {
                if (model.id == id)
                {
                    context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();

                    return CreatedAtRoute("GetSalones", new { id = model.id }, model);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
