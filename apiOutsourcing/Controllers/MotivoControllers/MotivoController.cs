﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Controlador Motivo ---

using apiOutsourcing.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Controllers.MotivoControllers
{
    [Route("api/Motivo")]
    [ApiController]
    public class MotivoController : ControllerBase
    {
        // GET: api/<MotivoController>
        private readonly AppDbContext context;

        public MotivoController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet(Name = "GetMotivo")]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Motivo.ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost(Name = "postMotivo")]
        public ActionResult Post([FromBody] Models.MotivoModels.MotivoModels model)
        {
            try
            {
                context.Motivo.Add(model);
                context.SaveChanges();

                return CreatedAtRoute("GetMotivo", new { id = model.id_Motivo}, model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Models.MotivoModels.MotivoModels model)
        {
            try
            {
                if (model.id_Motivo == id)
                {
                    context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();

                    return CreatedAtRoute("GetMotivo", new { id = model.id_Motivo }, model);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
