﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Controlador Departamentos ---

using apiOutsourcing.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Controllers.DepartamentosControllers
{
    [Route("api/Departamentos")]
    [ApiController]
    public class DepartamentosController : ControllerBase
    {
        // GET: api/<DepartamentosController>
        private readonly AppDbContext context;

        public DepartamentosController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet(Name = "GetDepartamentos")]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Departamentos.ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost(Name = "postDepartamentos")]
        public ActionResult Post([FromBody] Models.DepartamentosModels.DepartamentosModels model)
        {
            try
            {
                context.Departamentos.Add(model);
                context.SaveChanges();

                return CreatedAtRoute("GetDepartamentos", new { id = model.id_Departamentos }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Models.DepartamentosModels.DepartamentosModels model)
        {
            try
            {
                if (model.id_Departamentos == id)
                {
                    context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();

                    return CreatedAtRoute("GetDepartamentos", new { id = model.id_Departamentos}, model);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
