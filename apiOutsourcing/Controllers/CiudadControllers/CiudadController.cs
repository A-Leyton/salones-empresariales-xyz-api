﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Controlador Ciudad---

using apiOutsourcing.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiOutsourcing.Controllers.CiudadControllers
{
    [Route("api/Ciudad")]
    [ApiController]
    public class CiudadController : ControllerBase
    {
        // GET: api/<CiudadController>
        private readonly AppDbContext context;

        public CiudadController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet(Name = "GetCiduad")]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Ciudad.ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost(Name = "postCiudad")]
        public ActionResult Post([FromBody] Models.CiudadModels.CiudadModels model)
        {
            try
            {
                context.Ciudad.Add(model);
                context.SaveChanges();

                return CreatedAtRoute("GetCiudad", new { id = model.id_Ciudad }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Models.CiudadModels.CiudadModels model)
        {
            try
            {
                if (model.id_Ciudad == id)
                {
                    context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();

                    return CreatedAtRoute("GetCiudad", new { id = model.id_Ciudad }, model);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
