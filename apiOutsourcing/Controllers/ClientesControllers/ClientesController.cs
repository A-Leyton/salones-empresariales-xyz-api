﻿// --- Autor: Andres Leyton ---
// --- Fecha creación: 22-05-2021 ---
// --- Descripción: Controlador Clientes ---

using apiOutsourcing.Context;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Controllers.ClientesControllers
{
    [Route("api/Clientes")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly AppDbContext context;

        public ClientesController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet(Name = "GetClientes")]
        public ActionResult Get()
        {
            try
            {
                return Ok(context.Clientes.ToList());
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost(Name = "postClientes")]
        public ActionResult Post([FromBody] Models.ClientesModels.ClientesModels model)
        {
            try
            {
                context.Clientes.Add(model);
                context.SaveChanges();

                return CreatedAtRoute("GetClientes", new { id = model.id }, model);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Models.ClientesModels.ClientesModels model)
        {
            try
            {
                if (model.id == id)
                {
                    context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();

                    return CreatedAtRoute("GetClientes", new { id = model.id }, model);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
    }
}
