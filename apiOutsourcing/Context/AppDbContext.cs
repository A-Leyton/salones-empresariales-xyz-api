﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiOutsourcing.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Models.ClientesModels.ClientesModels> Clientes { get; set; }
        public DbSet<Models.SalonesEmpresarialesModels.SalonesModels> Salones { get; set; }
        public DbSet<Models.DepartamentosModels.DepartamentosModels> Departamentos { get; set; }
        public DbSet<Models.CiudadModels.CiudadModels> Ciudad { get; set; }
        public DbSet<Models.MotivoModels.MotivoModels> Motivo { get; set; }

    }
}
